class Product {
  final String rexelPartNumber;
  final String name;
  final String shortDescription;

  Product({this.rexelPartNumber,this.name,this.shortDescription});
  factory Product.fromJason(Map<String ,dynamic> json) {
    return Product(
      rexelPartNumber: json['rexelPartNumber'],
      name: json['name'],
      shortDescription: json['shortDescription']
    );
  }
}
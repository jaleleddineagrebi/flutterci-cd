import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:selfcheckout/controller/api.dart';
import 'package:selfcheckout/models/product.dart';


class ScanCode extends StatefulWidget {


  ScanCode ({Key  key,this.title}): super(key :key);
  final String title;
  @override
  ScanCodePage createState() {
    return new ScanCodePage();
  }
}

class ScanCodePage extends State<ScanCode> {
  String result = "Barcode";
  Future<Product> product;
  ScanCodePage ({Key  key,this.product});
  Api api = new Api();

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult;
        product = api.getProduct(qrResult);
      });
      api.getProduct(qrResult);
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
        });
      } else {
        setState(() {
          result = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        result = "Tu as clicker sur le boutton avant de scaner";
      });
    } catch (ex) {
      setState(() {
        result = "Unknown Error $ex";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("QR Scanner"),
      ),
      body: Center(
        child: FutureBuilder<Product>(
          future: product, 
          builder: ( context,  snapshot) 
          {
            if(snapshot.hasData){
              return ListTile(
                leading: FlutterLogo(size: 56.0),
                title: Text(snapshot.data.name,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                  color: Colors.blue
                ),
                
                ),
                subtitle: Text(snapshot.data.shortDescription),
            
              );
            } 
            return Text(result);
          },
          
          )
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.camera_alt),
        label: Text("Scan"),
        onPressed: _scanQR,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
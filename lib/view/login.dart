import 'package:flutter/material.dart';
import 'package:selfcheckout/controller/api.dart';
import 'package:selfcheckout/view/scanner.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget{

  LoginPage ({Key  key,this.title}): super(key :key);
  final String title;

  BuildContext get context => null;

  @override
  LoginPageState createState() => LoginPageState();
}
  
  class LoginPageState extends State<LoginPage>{
    Api api = new Api();
    String msgStatus;
    readToken() async{
          final prefs = await SharedPreferences.getInstance();
          final key = 'token';
          final value = prefs.get(key) ?? 0;
          if(value != null){
            Navigator.of(context).push(new MaterialPageRoute(builder :(BuildContext context)=> new ScanCode()));
          }
      }
      @override
      void initState() {
        readToken();
      }

    final TextEditingController  _usernamecontroller = new TextEditingController();
    final TextEditingController  _passwordcontroller = new TextEditingController();
   
    _onPressed(){
      setState(() {
       if(_usernamecontroller.text.trim().toLowerCase().isNotEmpty &&
          _passwordcontroller.text.trim().isNotEmpty ){
          api.getToken(_usernamecontroller.text.trim().toLowerCase(),
              _passwordcontroller.text.trim()).whenComplete((){
                if(api.status){
                  _showDialog();
                  msgStatus = 'Check email or password';
                }else{
                
                  Navigator.pushReplacementNamed(context, '/scancode');

                }
          });
      }
      });
    }
    @override
    Widget build(BuildContext context){
      return MaterialApp(
        title: 'Login Page',
        home: Scaffold(
          appBar:AppBar(
            title: new Text('Login')
          ), 
          body: Container(
            child: ListView(
            padding: const EdgeInsets.only(top: 62,left: 12.0,right: 12.0,bottom: 12.0),
            children: <Widget>[
              Container(
                height: 50,
                child: new TextField(
                  controller: _usernamecontroller,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'username',
                    hintText: 'Place your username',
                    icon: new Icon(Icons.supervised_user_circle),
                  ),
                ),
              ),
              Container(
                height: 50,
                child: new TextField(
                  controller: _passwordcontroller,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    labelText: 'password',
                    hintText: 'Place your password',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              new Padding(padding: new EdgeInsets.only(top: 44.0),),
              
              Container(
                height: 50,
                child: new RaisedButton(
                  onPressed: _onPressed,
                  color: Colors.blue,
                  child: new Text(
                    'Login',
                    style: new TextStyle(
                        color: Colors.white,
                        backgroundColor: Colors.blue),),
                ),
              ),
              new Padding(padding: new EdgeInsets.only(top: 44.0),),

              Container(
                height: 50,
                child: new Text(
                   '$msgStatus',
                   textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ]
            ),
          ),

          
          )
      );

    }

    void _showDialog(){
      showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: new Text('failed'),
            content: new Text('check your username or password'),
            actions: <Widget>[
              new RaisedButton(
                onPressed: (){Navigator.of(context).pop();},
                child: new Text('close'),
                )
            ],
          );
        }
      );
    }

  }

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:selfcheckout/flavors/AppConfig.dart';
import 'package:selfcheckout/flavors/sto/UatConfig.dart';

import 'package:selfcheckout/services/UserService.dart';
import 'package:selfcheckout/view/navigationBottom.dart';


class WebView extends StatefulWidget {
   
   UserService userService;
   WebView({Key key,@required this.userService}): super(key : key);
 
  @override
  WebViewState createState() => WebViewState(userService);
}

class WebViewState extends State<WebView>{

  
  UserService userService;

  WebViewState(this.userService);
    
  var configuredApp  = AppConfig(
    config: new UatConfig(),
  );
  FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();

  @override
   initState()  {
    super.initState();
    flutterWebviewPlugin.onStateChanged.listen((WebViewStateChanged wvs) {});    
  }

  @override
  Widget build(BuildContext context) {
    var appConf = AppConfig.of(context);
    return WebviewScaffold( 
       appBar: AppBar(
        title: Text('WebView'),
      ),
      url: appConf.webviewUrl,
      headers: {'Authorization': 'Basic UmV4ZWxVQVQ6UmV4ZWwkNTY='},
      withJavascript: true,
      bottomNavigationBar: NavigationBottom(),
    );
  }
}

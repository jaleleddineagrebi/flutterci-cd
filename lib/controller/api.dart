import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:selfcheckout/models/product.dart';

class Api {

    var status;
    var token;
     _saveToken(String token) async{
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = token;
    prefs.setString(key, value);
    }
   getToken(String username,String password) async {
    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
    IOClient ioClient = new IOClient(httpClient);

 
    final response = await ioClient.post("https://52.17.30.207/sto/api/oauth/token",
      headers: {
        'Accept': '*/*',
        //HttpHeaders.authorizationHeader: '',
        HttpHeaders.contentTypeHeader:"application/x-www-form-urlencoded",
        'Authorization': 'Basic bW9iaWxlOjdeWzlQMj9CQ2M='
        
      },
      body: {
        "client_id":"mobile_android",
        "client_secret":"secret",
        "grant_type":"password",
        "username":"$username",
        "password":"$password",
      }
    );
      status = response.body.contains('error');
      var data = json.decode(response.body);
      if(status){
        print('data: ${data["error"]}');
      }else{
        print('data: ${data["tokens"]["access_token"]}');
        _saveToken(data["tokens"]["access_token"]);
      }

   }
    readToken() async{
      final prefs = await SharedPreferences.getInstance();
      final key = 'token';
      final value = prefs.get(key) ?? 0;
    }

    Future<Product>getProduct(String code) async{
      bool trustSelfSigned = true;
      HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => trustSelfSigned);
      IOClient ioClient = new IOClient(httpClient);
      final prefs = await SharedPreferences.getInstance();
      final key = 'token';
      final value = prefs.get(key) ?? 0;
      String urlVar = "getProductByCode?code";
      int Qrcode = int.parse(code);
      if(code.length == 12){
          urlVar = "getProductByBarcode?barcode";
      }
      final response = await ioClient.get("https://52.17.30.207/sto/products/$urlVar=$Qrcode",
        headers: {
          'Accept': '*/*',
          //HttpHeaders.authorizationHeader: '',
          HttpHeaders.contentTypeHeader:"application/x-www-form-urlencoded",
          'Authorization': 'Basic bW9iaWxlOjdeWzlQMj9CQ2M='
          
        },
      );
      status = response.body.contains('error');
      var data = json.decode(response.body);
      if(!status){
        print("data : $data");
        return Product.fromJason(json.decode(response.body));
      }
     
    }
    

}





import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:selfcheckout/view/login.dart';
import 'package:selfcheckout/view/scanner.dart';
import 'package:appcenter/appcenter.dart';
import 'package:appcenter_analytics/appcenter_analytics.dart';
import 'package:appcenter_crashes/appcenter_crashes.dart';



void main() => runApp(MyApp());

@override
class MyApp extends StatefulWidget{
  @override
  _MyAppState createState() => _MyAppState();
  final String title = 'home';

  

 

}
class _MyAppState extends State<MyApp> {
  
  String _appSecret;


  _MyAppState() {
    final ios = defaultTargetPlatform == TargetPlatform.iOS;
    _appSecret = ios ? "a850bd13-f2bf-4ddc-8522-5127fc6eef96" : "241d3170-3d15-4e78-8341-adadd29dc437";
  }

  @override
  initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  initPlatformState() async {
    await AppCenter.start(
        _appSecret, [AppCenterAnalytics.id, AppCenterCrashes.id]);


  }

  @override
  Widget build(BuildContext context){
    return MaterialApp(
    title: 'Selfcheckout',
    theme: ThemeData(
      primarySwatch: Colors.blue
    ),
    home: LoginPage(
      title: 'login Page'
    ),
    routes: <String, WidgetBuilder>{
     '/scancode' : (BuildContext context) => new ScanCode(),
    },
  );
  }
}


  





